#!/bin/bash

. ../.env

#check if API is already exists in MIFE
CHECK_URL=$MIFE_URL?query=context:$context
echo "curl --silent --write-out \"HTTPSTATUS:%{http_code}\" -k  -v -X GET \"$CHECK_URL\" -H \"Authorization: Bearer $MIFE_TOKEN\" -H \"Content-Type: application/json\""

CHECK_API=$(curl --silent --write-out "HTTPSTATUS:%{http_code}" -k  -v -X GET "$CHECK_URL" -H "Authorization: Bearer $MIFE_TOKEN" -H "Content-Type: application/json" )
CHECK_BODY=$(echo "$CHECK_API" | sed -e 's/HTTPSTATUS:.*//g')
CHECK_STATUS=$(echo "$CHECK_API" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')
echo $CHECK_BODY

MIFE_API_ID=$(echo "$CHECK_BODY" | jq .list[0].id | tr -d '"')

if [[ "$CHECK_STATUS" -eq 200 ]]
then
    count=$(echo "$CHECK_BODY" | jq .count)
    if [[ "$count" -eq 0 ]]
    then
        echo "-------------------------------"
  		echo "No APIs found in same conext to Delete....."
		echo "-------------------------------"
		exit 1
   else
	   
        echo "APIs found, Deleting API....."
        echo "-------------------------------"
        echo "curl --silent --write-out \"HTTPSTATUS:%{http_code}\" -k  -v -X DELETE \"$MIFE_URL/$MIFE_API_ID\" -H \"Authorization: Bearer $MIFE_TOKEN\""
		echo "-------------------------------"

		DELETE_API=$(curl --silent --write-out "HTTPSTATUS:%{http_code}" -k  -v -X DELETE "$MIFE_URL/$MIFE_API_ID" -H "Authorization: Bearer $MIFE_TOKEN")
		DELETE_STATUS=$(echo "$DELETE_API" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')
		echo $DELETE_STATUS

        if [[ "$CHECK_STATUS" -eq 200 ]]
        then 
            echo "API is Successfully deleted"
        else 
            echo "API deleting is failed"
            exit 1
        fi
	fi
else
   echo "Response of query API : $CHECK_STATUS"
   echo ""
   echo "$CHECK_BODY"
   exit 1
fi
